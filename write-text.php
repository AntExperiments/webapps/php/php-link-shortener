<?php
    $p = "";
    do {
        // generate a unique file path
        $p = 't/' . substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5) . '.html';
    } while (file_exists($p));

    $text = $_POST['text'];
    $myfile = fopen($p, "w") or die("Unable to open file!");

    fwrite($myfile, "<!DOCTYPE html>
    <html>
        <head>
            <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css\"><link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">
    
            <title>Ant Link and Text Shortener</title>
    
            <script defer src=\"script.js\"></script>
        </head>
        <body>
            <nav>
                <div class=\"nav-wrapper\">
                    <a href=\"../index.html\" class=\"brand-logo center\">Ant Link shortener</a>
                </div>
            </nav>
    
            <div style=\"height: 20px\"></div>
    
            <div class=\"container\">
                <div class=\"row\">
                    <p>$text</p>
                </div>
            </div>
    
            <script src=\"https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js\"></script>
        </body>
    </html>");

    fclose($myfile);
    
    header("Location: $p");
?>